import {Jumbotron,Row,Col} from 'react-bootstrap'

//import Link from next.js
import Link from 'next/link'

export default function Banner({dataProp}){

	//What is the data type of dataProp? object
	const {title,description,destination,label} = dataProp

	/*
		We will make this Banner component reusable for other pages by passing data from a parent component to a child component:

			With the use of passing Props.
	*/

	return (

			<Row>
				<Col>
					<Jumbotron>
						<h1>{title}</h1>
						<p>{description}</p>
						<Link href={destination}>
							<a>{label}</a>
						</Link>
					</Jumbotron>
				</Col>
			</Row>

		)

}
