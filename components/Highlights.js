import React from 'react'
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Highlights(){
	return(
		<Row>
			<Col>
				<Card>
				  <Card.Body>
				    <Card.Title>Track Expense from Home</Card.Title>
				    <Card.Text>
				      Some quick example text to build on the card title and make up the bulk of
				      the card's content.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col>
				<Card>
				  <Card.Body>
				    <Card.Title>Loan Now, Pay Later</Card.Title>
				    <Card.Text>
				      Some quick example text to build on the card title and make up the bulk of
				      the card's content.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
			<Col>
				<Card>
				  <Card.Body>
				    <Card.Title>About App</Card.Title>
				    <Card.Text>
				      Some quick example text to build on the card title and make up the bulk of
				      the card's content.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>

		)
}
