import {useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'

//import Link component from nextjs. Nextjs has its own implementation of the Link component
import Link from 'next/link'

//import UserContext to have access to our global user state in this component.
import UserContext from '../UserContext'
export default function NavBar(){

	//destructure the UserContext after it's been unwrapped by the useContext hook so that we may access to the values of the context.
	const {user} = useContext(UserContext)

	//using the global user state, we will conditionally render our links, so that when the user is logged in, they will not see the login and register link anymore.
	return (

			<Navbar className="navbar navbar-expand-lg navbar-dark bg-dark" expand="lg">
				<Link href='/'>
					<a className="navbar-brand" >Budget-Tracker</a>
				</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
						{	
							//access global user state's email property as condition to the rendering of the links
							user.id !== null
							? 
								<>
									<Link href="/category">
											<a className="nav-link" role="button">Category
											</a>
									</Link>
									<Link href="/search">
											<a className="nav-link" role="button">Record
											</a>
									</Link>

									<Link href='/income'>
										<a className="nav-link" role="button">
											Monthly Income
										</a>
									</Link>
									
									<Link href='/expense'>
										<a className="nav-link" role="button">
											Monthly Expense
										</a>
									</Link>

									<Link href='/logout'>
										<a className="nav-link" role="button">
											Logout
										</a>
									</Link>
								</>
								:
								<>
								<Link href='/login'>
									<a className="nav-link" role="button">
										Login
									</a>
								</Link>
								</>

							}

					</Nav>
				</Navbar.Collapse>
			</Navbar>

		)

}
