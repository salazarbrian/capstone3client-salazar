import {useState,useEffect,Fragment} from 'react'
import {Bar} from 'react-chartjs-2'

import moment from 'moment'

export default function MonthlyExpense({rawData}){

	const [months,setMonths] = useState(

		[
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
		]

		)
	const [expensePerMonth,setExpensePerMonth] = useState([])

	useEffect(()=>{

		//compute the total sales of a particular month and set it in our state: salesPerMonth

		setExpensePerMonth(months.map(month => {

			//We will accumulate the sales for the current month into this variable.
			let expensePerMonth = 0


			rawData.forEach(element => {

				//Each item in the rawData array will be iterated.
				//months[0] -> forEach must finish first -> months[1]
				//element.sale_date: "MM/DD/YYYY" or "10/19/2019"
				if(moment(element.dateOfTransaction).format("MMMM") === month){

					//If the current entry falls on the current month being iterated by the map, we will accumulate its sales
					expensePerMonth += parseInt(element.amount)
				}

			})

			return expensePerMonth

		}))

	},[rawData])

	console.log(expensePerMonth)

	const data = {

		labels: months, //x-axis label
		datasets: [{

			label: 'Monthly Expense in PHP',//label for the bar
			backgroundColor: 'Pink',
			borderColor: 'white',
			borderWidth: 1,
			hoverBackgroundColor: 'Red',
			hoverBorderColor: 'black',
			data: expensePerMonth

		}]


	}
	const options = {

		scales: {
			yAxes:[
				{

					ticks: {
						beginAtZero: true
					}		
				}
			]
		}

	}

	return (	
		<Fragment>
			<h1>Monthly Expense in PHP</h1>
			<Bar data={data} options={options}/>
		</Fragment>

		)

}
