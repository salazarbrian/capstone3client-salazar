/*
	This file's path is pages/courses/index.js, with the use of nextjs routing, what will be the endpoint/route for this page?

	http://localhost:3000/courses/

*/
import {useEffect,useState,useContext} from 'react'
import {Table,Button,Form} from 'react-bootstrap'
import Swal from 'sweetalert2';
import UserContext from '../../UserContext'
import Router from 'next/router'


export default function Category(){
	const [name,setName] = useState("")
	const [type,setType] = useState("")
	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

		if(name !== "" && type !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[name,type]) 


	function addCategory(e){

		//prevent the refreshing of the page due to submit event:
		e.preventDefault()

		let token = localStorage.getItem('token')
		//Add a fetch request to create a new course:
		fetch('http://localhost:4000/api/categories/',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`

			},
			body: JSON.stringify({

				name: name,
				type: type

			})
		})
		.then(res => res.json())//process our server's response
		.then(data => {

			console.log(data)//What is the data type responded by our server? Boolean. It determines if we have created a new course or if there was an error.

			//Mini Activity:
			//Create a sweetalert for the following situations:
			//Show a sweetalert if the returned boolean is true to show that the course has been created.
			//Show a sweetalert if the returned boolean is false to show error in course creation
			if(data){
				if(data.name.toLowerCase() !== name.toLowerCase){
					console.log(data.name)
				Swal.fire({

					icon: 'success',
					title: 'Course Created.',
					text: 'Successful Course Creation.'

				})

				Router.push('/category')
	
			 }else {

				Swal.fire({

					icon: 'error',
					title: 'Course Creation Failed.',
					text: 'Internal Server Error.'

				})

			}
 		}

		})

		setName("")
		setType("")

	}

	return (

			<>
				<div className = "mt-5 pt-4 mb-5 container">
					<div className = "justify-content-center row">
						<div className="col-md-6 col">
							<h3>New Category</h3>
							<div className="card">
								<div className = "card-header">Category Information</div>
								<div className="card-body">
								<Form onSubmit={ e => addCategory(e)}>

								 <Form.Group>
									<Form.Label>Category Name:</Form.Label>
									<Form.Control 
										type="text"
										placeholder="Enter category name"
										value={name}
										onChange={(e) => setName(e.target.value)}
										required
					                />
								</Form.Group>

								<Form.Group>
									<Form.Label>Category Type:</Form.Label>
								      <Form.Control as="select" defaultValue="Select Category" onChange={(e) => setType(e.target.value)}>
								      	<option disabled>Select Category</option>
								        <option value="Income">Income</option>
								        <option value="Expense">Expense</option>
								       </Form.Control>
								</Form.Group>

															{
											isActive 
											? <Button type="submit" variant="primary">Submit</Button>
											: <Button type="submit" variant="danger" disabled>Submit</Button>
										}
								</Form>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</>

		)


}


