
import {useEffect,useState,useContext} from 'react'
import {Table,Button} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../../UserContext'


export default function Category(){

	const {user} = useContext(UserContext) 

	console.log(user)
	const [category,setCategory] = useState([])
	useEffect(()=>{
		 const local = localStorage.getItem('token')
		 console.log(local)
		fetch('http://localhost:4000/api/categories/display', {

                    headers: {

                        Authorization: `Bearer ${local}`

                    }

                })
		.then(res => res.json())
		.then(data => {
			
			setCategory(data)
		})

	},[])


		const categoryRows = category.filter(data => data.user === user.id).map(categories => {
		//Create a list of table row react elements

		return (
				
				<tr key={categories._id}>

					<td>{categories.name}</td>
					<td>{categories.type}</td>
				</tr>
			)
	})




	return (

			<>
			<div className = "container">
				<div className = "mt-5 pt-4 mb-5 container">
					<h3>Category</h3>
				<Link href='/category/add'>
				<a className="btn btn-success mt-1 mb-3 mr-3" role="button">
						Add
						</a>
				</Link>
				<Link href='/category/user'>
				<a className="btn btn-info mt-1 mb-3" role="button">
						Info
						</a>
				</Link>
					<Table className = "table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Category</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							{categoryRows}
						</tbody>
					</Table>
				</div>
			</div>
			</>

		)


}
