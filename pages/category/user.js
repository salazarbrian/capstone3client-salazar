
import {useEffect,useState,useContext} from 'react'
import {Table,Button} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../../UserContext'


export default function Category(){

	const {user} = useContext(UserContext) 

	console.log(user)
	const [category,setCategory] = useState([])
	useEffect(()=>{
		 const local = localStorage.getItem('token')
		 console.log(local)
		fetch('http://localhost:4000/api/users/details', {

                    headers: {

                        Authorization: `Bearer ${local}`

                    }

                })
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCategory(data)
})
	},[])

		console.log(setCategory)






	return (

			<>
			<div className = "container">
				<div className = "mt-5 pt-4 mb-5 container">
					<h3>Category</h3>
				<Link href='/category/add'>
				<a className="btn btn-success mt-1 mb-3 mr-3" role="button">
						Add
						</a>
				</Link>
					<Table className = "table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Name</th>
								<th>Amount</th>
							</tr>
						</thead>
								<tr>
								<td>{category.firstName} {category.lastName}</td>
								{
									category.savings === 0
									?
									<td>{category.savings}</td>
									:
									category.savings <= 0
									?
									<td className="text-danger">{category.savings}</td>
									:
									<td className="text-success">+{category.savings}</td>
								}
							</tr>
						<tbody>
						</tbody>
					</Table>
				</div>
			</div>
			</>

		)


}
