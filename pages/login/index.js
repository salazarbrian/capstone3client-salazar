import React, { useState, useEffect,useContext } from 'react';

import Router from 'next/router';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Link from 'next/link'
import UserContext from '../../UserContext';

//import Google Login - a component from react google login which will be the component to use for our google login button.
import {GoogleLogin} from 'react-google-login'

//client id 90581413699-ip64vs1bng77evmonv9gbeq064qu3n7h.apps.googleusercontent.com

export default function index() {
    //useContext will return an object which contains the values passed in the UserProvider.
    const {user,setUser} = useContext(UserContext)

    // console.log(user)

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();
        fetch('http://localhost:4000/api/users/login', {
       method: "POST",
            headers: {
                'Content-Type': "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data.accessToken){

                //Store the token into the localStorage while logging in.
                localStorage.setItem('token', data.accessToken)


                fetch('http://localhost:4000/api/users/details',{


                    headers: {

                        Authorization: `Bearer ${data.accessToken}`

                    }

                })
                .then(res => res.json())
                .then(data => {

                    //console.log(data)
                    //Set our user's data/details from our db to our localStorage so that we may also update the global user state.
                    localStorage.setItem('id',data._id)
                    localStorage.setItem('isAdmin',data.isAdmin)

                    setUser({

                        id: data._id,
                        isAdmin: data.isAdmin

                    })
                })

                setEmail('');
                setPassword('');

                Swal.fire({
                    icon: "success",
                    title: "Successfully logged in.",
                    text: "Thank you for logging in."
                })

                //push method from router component will allow us to redirect the user to our desired page.
                Router.push('/')

            } else {

                Swal.fire({
                    icon: "error",
                    title: "Unsuccessful.",
                    text: "User authentication failed."
                })

            }

        })

    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);



    function authenticateGoogleToken(response){

        console.log(response)/// Includes accesstoken from Google, tokenId which can be verified for legitimacy in the backend (API) as we save our google login user's details in the db AND if they are already registered we can provide the user our app's OWN accessToken to login our new google user into our APP.

        fetch('http://localhost:4000/api/users/verify-google-id-token',{


            method: 'POST',
            headers: {

                'Content-Type': 'application/json'

            },
            body: JSON.stringify({

                tokenId: response.tokenId

            })

        })
        .then(res => res.json())
        .then(data => {
            
            console.log(data);

            // typeof data.accessToken !== 'undefined'
            // 'object' !== 'undefined' - true
            // undefined === 'undefined' - false

            if (typeof data.accessToken !== 'undefined') {

                localStorage.setItem('token', data.accessToken)
                // console.log("Successful login");
                retrieveUserDetails(data.accessToken);

                Router.push('/')

            } else {

                if(data.error === 'google-auth-error'){
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error'
                    )
                } else if (data.error === 'login-type-error'){
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure',
                        'error'
                    )
                }

            }
        })

    }

    function retrieveUserDetails(accessToken) {
        fetch('http://localhost:4000/api/users/details', {
       headers: { Authorization: `Bearer ${accessToken}` }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            localStorage.setItem('id', data._id)
            localStorage.setItem('isAdmin', data.isAdmin)

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })

        })

    }

    /*
        Google Login Button - is a react component use for google login through the npm package: react-google-login.

        clientId = OAuthClient id from Cloud Google Developers platform used for authorizing the use of Google API and our Google AuthClient for authenticating our google login users.

        onSuccess = allows us to run a function which on successful google login, returns a googleuser object which provides an tokenId, an accessToken and some google user details. TokenId later will be used to verify the legitimacy of the google login and get our user's details to save our new user into the app's db.
        
        onFailure = allows to run a function which is either called or ran when initialization of google login fails (Ex. client id is wrong or has an error) or there was an error in the google login attempt.

        cookiePolicy = determines the cookie policy for the origin of the google login request.

        buttonText = changeable/mutable string for the use of google login button text.
    */

    return (
                <div className="container">
                    <div className="mt-5 pt-4 mb-5 container">
                        <div className="justify-content-center row">
                            <div className="col-md-6 col">
                                <h3>My Budget Tracking</h3>
                                <p>Login in by using your Google Account or your Registered Email.</p>
                                <Form onSubmit={(e) => authenticate(e)}>
                                    <Form.Group controlId="userEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password"
                                    value={password}
                                    onChange={(e) => setPassword(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            {isActive ? 
                                <Button variant="primary" type="submit" id="submitBtn" className="btn-block mb-3 btn btn-primary">
                                    Submit
                                </Button>
                                : 
                                <Button variant="danger" type="submit" id="submitBtn" disabled className="btn-block mb-3 btn btn-danger">
                                    Submit
                                </Button>
                            }
                            <GoogleLogin 

                                clientId="90581413699-ip64vs1bng77evmonv9gbeq064qu3n7h.apps.googleusercontent.com"
                                buttonText="Login Using Google"
                                onSuccess={authenticateGoogleToken}
                                onFailure={authenticateGoogleToken}
                                cookiePolicy={'single_host_origin'}
                                className="w-100 text-center my-4 d-flex justify-content-center"
                            />
                                </Form>
                            <p className="text-center mt-2">
                            If you haven't registered yet ,  
                             <Link href='/register'>
                                <a role="button">
                                         Sign Up Here!
                                        </a>
                                </Link>
                            </p>
                            </div>
                        </div>
                    </div>
                </div>

    )
}
