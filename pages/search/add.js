/*
	This file's path is pages/courses/index.js, with the use of nextjs routing, what will be the endpoint/route for this page?

	http://localhost:3000/courses/

*/
import {useEffect,useState,useContext} from 'react'
import {Form,Button} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../../UserContext'
import Swal from 'sweetalert2';
import Router from 'next/router'


export default function addSearch({data}){
	const {user} = useContext(UserContext)
	const [type,setType] = useState("true")
	const [name,setName] = useState("")
	const [amount,setAmount] = useState(0)
	const [description,setDescription] = useState("")
	const [allCategories,setCategories] = useState([])
console.log(data)
	
	const [isActive,setIsActive] = useState(true)

	const newData = data.filter( categ => categ.user === user.id)


	useEffect(()=>{

		if(name !== "" && description !== "" && amount !== 0 && type !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[name, description,amount,type]) 

	function addTransaction(e){

		//prevent the refreshing of the page due to submit event:
		e.preventDefault()
		let token = localStorage.getItem('token');
		//Add a fetch request to create a new course:
		console.log(token)
		fetch('http://localhost:4000/api/ledger/',{ 

			method: 'POST',
			headers: {

				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`

			},
			body: JSON.stringify({

				name: name,
				type: type,
				amount: amount,
				description: description
	
			})
		})
		.then(res => res.json())//process our server's response
		.then(data => {

			
			if(data){

				Swal.fire({

					icon: 'success',
					title: 'Course Created.',
					text: 'Successful Course Creation.'

				})

				Router.push('/search')
				//Redirect our user to the courses page after course creation.

			} else {

				Swal.fire({

					icon: 'error',
					title: 'Course Creation Failed.',
					text: 'There has been an internal server error.'

				})
			}
		})


		setType("")
		setName("")
		setDescription("")
		setAmount(0)

	}

const incomeType = newData.filter( data => data.type === 'Income').map( data => {
    return (
        <option key={data._id} value={data.name}>
            {data.name}
        </option>
    )
})

const expenseType = newData.filter( data => data.type === 'Expense').map( data => {
    return (
        <option key={data._id} value={data.name}>
            {data.name}
        </option>
    )
})

    console.log(expenseType)
	return (
	<div className="container">
	<div className="mt-5 pt-4 mb-5 container">
		<div className="justify-content-center row">
			<div className="col-md-6 col">
				<h3>New Record</h3>
				<div className="card">
					<div className="card-header">Record Information</div>
					<div className= "card-body">
					 <Form onSubmit={ e => addTransaction(e)}>
						<Form.Group>
			                    <Form.Label>Category Type:</Form.Label>
			                    <Form.Control 
			                    as="select" 
			                    value={type}
			                    onChange={ (e) => setType(e.target.value)}
			                    required
			                    >
			                        <option value='true' disabled>Select</option>
			                        <option>Income</option>
			                        <option>Expense</option>
			                    </Form.Control>
			                </Form.Group>
			               {
			               	type === "Income"
			               	?	<Form.Group>
				                        <Form.Label>Category Name:</Form.Label>
				                        <Form.Control 
				                        as="select" 
				                        value={name}
				                        onChange={ (e) => setName(e.target.value)}
				                        required
				                        >
				                            <option value='true'>Select</option>
				                            {incomeType}
				                        </Form.Control>
				                 </Form.Group>
			                 :   <Form.Group>
			                        <Form.Label>Category Name:</Form.Label>
			                        <Form.Control 
			                        as="select" 
			                        value={name}
			                        onChange={ (e) => setName(e.target.value)}
			                        required
			                        >
			                            <option value='true'>Select</option>
			                            {expenseType}
			                        </Form.Control>
			                  </Form.Group>
			               }
			                <Form.Group>
			                    <Form.Label>Amount:</Form.Label>
			                        <Form.Control 
			                            type="number" 
			                            placeholder="Enter amount"
			                            value={amount}
			                            onChange={ (e) => setAmount(e.target.value)}
			                            required
			                        />
			                </Form.Group>

			                <Form.Group>
			                    <Form.Label>Description:</Form.Label>
			                        <Form.Control 
			                            type="text" 
			                            placeholder="Enter Description"
			                            value={description}
			                            onChange={ (e) => setDescription(e.target.value)}
			                            required
			                        />
			                </Form.Group>
			                <Button type="submit" variant='primary'>Add</Button>
			            </Form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		)
}

export async function getStaticProps() {
    const res = await fetch(`http://localhost:4000/api/categories/display`)
    const data = await res.json()
  
    if (!data) {
      return {
        notFound: true,
      }
    }
  
    return {
      props: { data }, // will be passed to the page component as props
    }
  }
