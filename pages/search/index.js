import {useEffect,useState,useContext} from 'react'
import {Form,Button} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../../UserContext'
import moment from 'moment'
export default function Search({data}){

	const {user} = useContext(UserContext) 
	const [ledger,setLedger] = useState([])
	const [ExpenseArray,setExpenseArray] = useState([])
	const [IncomeArray,setIncomeArray] = useState([])
	const [type,setType] = useState("true")

	const [targertSearch, setSearch] = useState("")

	const [category,setCategory] = useState([])
	const [ExpenseArrayLedger,setExpenseArrayLedger] = useState([])
	const [IncomeArrayLedger,setIncomeArrayLedger] = useState([])
	const [categoryLedger,setCategoryLedger] = useState([])

	const [allData, setAllData] = useState([])
	const newData = data.filter( categ => categ.user === user.id)
	console.log(data)


	// const [ExpenseArrayLedger,setExpenseArrayLedger] = useState([])
	// const [IncomeArrayLedger,setIncomeArrayLedger] = useState([])
	// const [categoryLedger,setCategoryLedger] = useState([])

	useEffect(()=>{
		const local = localStorage.getItem('token')
		 console.log(local)

		fetch('http://localhost:4000/api/ledger/display', {

                    headers: {

                        Authorization: `Bearer ${local}`

                    }

                })
		.then(res => res.json())
		.then(data => {
		setAllData(data)
	 	setCategoryLedger(data.map(categories => {
			    return (
				<div className="mb-3 card">
					<div className="card-body">
						<div className="row">
							<div key={categories._id} className="col-6">
								<h5>{categories.name}</h5>
								<h6>
								<span className="text-danger">{categories.type}</span>
								({categories.description})
								</h6>
								<p>{categories.createdON}</p>
							</div>
							<div className="text-right col-6">
								<h6 className="text-danger">{categories.amount}</h6>
							</div>
						</div>
					</div>
				</div>

			    )
			})
			)


		setExpenseArrayLedger(data.filter(category => {
				return category.type === "Expense"
			}).map(categories => {
		    return (
			<div className="mb-3 card">
				<div className="card-body">
					<div className="row">
						<div key={categories._id} className="col-6">
							<h5>{categories.name}</h5>
							<h6>
							<span className="text-danger">{categories.type}</span>
							({categories.description})
							</h6>
							<p>{categories.createdON}</p>
						</div>
						<div className="text-right col-6">
							<h6 className="text-danger">{categories.amount}</h6>
						</div>
					</div>
				</div>
			</div>

		    )
		}))

		setIncomeArrayLedger(data.filter(category => {
				return category.type === "Income"
			}).map(categories => {
			console.log(categories)
		    return (
			<div className="mb-3 card">
				<div className="card-body">
					<div className="row">
						<div key={categories._id} className="col-6">
							<h5>{categories.name}</h5>
							<h6>
							<span className="text-danger">{categories.type}</span>
							({categories.description})
							</h6>
							<p>{categories.createdON}</p>
						</div>
						<div className="text-right col-6">
							<h6 className="text-danger">{categories.amount}</h6>
						</div>
					</div>
				</div>
			</div>

		    )
		}))

		})
		
},[])

const showSearched = newData.filter( data => {
        return data.description.toLowerCase().includes(targertSearch)
    }).map( data => {
        let nameCapitalized = data.name.charAt(0).toUpperCase() + data.name.slice(1);
        let date = moment(data.createdOn).calendar();
        return (
			<div className="mb-3 card">
				<div className="card-body">
					<div className="row">
						<div key={data._id} className="col-6">
							<h5>{nameCapitalized}</h5>
							<h6>
							{
							data.type === "Income"
							?
							<span className="text-success">{data.type}</span>
							:
							<span className="text-danger">{data.type}</span>
							}
							({data.description})
							</h6>
							<p>{date}</p>
						</div>
						{
						data.type === "Income"
						?
						<div className="text-right col-6">
							<h6 className="text-success">+{data.amount}</h6>
						</div>
						:
						<div className="text-right col-6">
							<h6 className="text-danger">-{data.amount}</h6>
						</div>
						}
					</div>
				</div>
			</div>
        )
    })

const searchFilterIncome =  newData.filter( data => {
	if(data.type === "Income"){
        return data.description.toLowerCase().includes(targertSearch)
    }}).map( data => {
        let nameCapitalized = data.name.charAt(0).toUpperCase() + data.name.slice(1);
        let date = moment(data.createdOn).calendar();
        return (
			<div className="mb-3 card">
				<div className="card-body">
					<div className="row">
						<div key={data._id} className="col-6">
							<h5>{nameCapitalized}</h5>
							<h6>
							<span className="text-success">{data.type}</span>
							({data.description})
							</h6>
							<p>{date}</p>
						</div>
						<div className="text-right col-6">
							<h6 className="text-success">+{data.amount}</h6>
						</div>
					</div>
				</div>
			</div>
        )
    })
console.log(data)
console.log(searchFilterIncome)

const searchFilterExpense = newData.filter( data => {
 	if(data.type === "Expense"){
        return data.description.toLowerCase().includes(targertSearch)
    }}).map( data => {
        let nameCapitalized = data.name.charAt(0).toUpperCase() + data.name.slice(1);
        let date = moment(data.createdOn).calendar();
        return (
				 <div className="mb-3 card">
					<div className="card-body">
						<div className="row">
							<div key={data._id} className="col-6">
								<h5>{nameCapitalized}</h5>
								<h6>
								<span className="text-danger">{data.type}</span>
								({data.description})
								</h6>
								<p>{date}</p>
							</div>
							<div className="text-right col-6">
								<h6 className="text-danger">-{data.amount}</h6>
							</div>
						</div>
					</div>
				</div>
        )
    })
  console.log(showSearched)
           
//=========================================Return=========================================================
console.log(ExpenseArrayLedger)
	return (
			<>
<div className="container">
	<div className="mt-5 pt-4 mb-5 container">
		<h3>Records</h3>
		<div className="mb-2 input-group">
			<div className="input-group-prepend">
			<Link href='/search/add'>
				<a className="btn btn-success" role="button">
				Add
				</a>
			</Link>
			</div>

				<Form.Control
						type= "text"
						placeholder ="Search Record"
						value={targertSearch}
						onChange={e => setSearch(e.target.value)}
					/>		
	
				 <Form.Control as="select" defaultValue="true" onChange={(e) => setType(e.target.value)}>
			      	<option value="true">All</option>
			        <option value="Income">Income</option>
			        <option value="Expense">Expense</option>
			       </Form.Control>
			       </div>
					{
						
					    type ==="true" 
					    ?   targertSearch === 0
					        ? categoryLedger
					        : showSearched
					    : type === "Income"
					        ?targertSearch === 0
					            ?   IncomeArrayLedger
					            :   searchFilterIncome
					        :   targertSearch === 0
					            ?   ExpenseArrayLedger
					            :   searchFilterExpense
					

					}			
	</div>
</div>
		</>
	)

}

export async function getStaticProps() {
    const res = await fetch('http://localhost:4000/api/ledger/display')
    const data = await res.json()
  
    if (!data) {
      return {
        notFound: true,
      }
    }
  
    return {
      props: { data }, // will be passed to the page component as props
    }
  }
