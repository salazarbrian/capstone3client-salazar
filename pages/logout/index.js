import {useContext,useEffect} from 'react'
import UserContext from '../../UserContext'

//import Router component from nextjs to redirect our user after logging out:
import Router from 'next/router'

export default function Logout(){
	//Unwrap the UserContext using the useContext hook and get our unsetUser function.
	const {unsetUser} = useContext(UserContext)

	//use a useEffect to run the unsetUser function on initial render only.
	useEffect(()=>{

		unsetUser()

		Router.push('/login')


	},[])

	//A component should always return something.
	return null
}
