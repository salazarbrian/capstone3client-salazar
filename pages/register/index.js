

import React,{ useState,useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

//import Router from next/router so that we can redirect our users.
import Router from 'next/router'

export default function Register(){
	//create states to be used for input elements
	const [firstName,setFirstName] = useState("");
	const [lastName,setLastName] = useState("");
	const [mobileNo,setMobileNo] = useState(0);
	const [email,setEmail] = useState("");
	const [password1,setPassword1] = useState("");
	const [password2,setPassword2] = useState("");
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);


	function registerUser(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		//Post Method:
		fetch('http://localhost:4000/api/users/exist',{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'

			},
			body: JSON.stringify({

				email: email

			})

		})
		.then(res => res.json()) //process our server's response
		.then(data => {

			
			if(data === false){

				
				fetch('http://localhost:4000/api/users/',{

					method: 'POST',
					headers: {
						//If the request has a body, you must pass a content-type header.
						'Content-Type': 'application/json'

					},
					body: JSON.stringify({
						//If you miss one, you may have an error the backend.
						//Pass the input states as values for the key.
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password1

					})

				})
				.then(res => res.json())
				.then(data => {

					if(data === true){

						//if the received data is true after registering, we therefore, registered the user successfully, we can now show him a sweetalert and redirect the user to the login page.

						Swal.fire({

							icon: 'success',
							title: 'Registration Successful.',
							text: 'Thank you for registering.'

						})

						//What method from Router can we use to redirect the user?
						//A: push() method will allow us to redirect the user
						Router.push('/login')


					} else {

						//data received false after registering, therefore a server error within mongodb/mongoose was done and the user has not been registered.
						Swal.fire({

							icon: 'error',
							title: 'User Registration Failed.',
							text: 'An internal server error has occured.'

						})

					}

				})

			} else {

				//if Data response is true from email-exists show a sweetalert that user has registered with the same email.
				Swal.fire({

					icon: 'error',
					title: 'Registration Failed.',
					text: 'Email has already been registered.'

				})

			}


		})

		//clear the input fields back to its initial values.
		setFirstName("")
		setLastName("")
		setMobileNo(0)
		setEmail("")
		setPassword1("")
		setPassword2("")
		

	}

	useEffect(() => {

		if((firstName !=='' && lastName !== '' && mobileNo !== 0 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstName,lastName,mobileNo,email, password1, password2]);



	console.log(email)

	return (
		<div className="container">
		<h1 className="text-center">Register</h1>
		<Form className= "mb-3" onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
				<Form.Label>First Name:</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter First Name"
					value={firstName}
					onChange={(e) => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId="lastName">
				<Form.Label>Last Name:</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Last Name"
					value={lastName}
					onChange={(e) => setLastName(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control 
					type="number"
					value={mobileNo}
					onChange={(e) => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Password"
					value={password1}
					onChange={(e) => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={(e) => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ? 
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" disabled>
					Submit
				</Button>
			}
			
		</Form>
	</div>
	)

}
