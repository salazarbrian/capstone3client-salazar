import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {

		title: "My Budget-Tracker",
		description: "This Budget-Tracker uses Next.js framework",
		destination: '/' ,
		label: "Welcome Home"

	}

  return (
  	<React.Fragment>
       	<Banner dataProp={data}/>
       	<Highlights />
    </React.Fragment>
  )


}
