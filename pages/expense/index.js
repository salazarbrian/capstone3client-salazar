
import {useEffect,useState,useContext} from 'react'
import {Table,Button} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../../UserContext'
import MonthlyExpense from '../../components/MonthlyExpense'


export default function expense(){

  const {user} = useContext(UserContext) 

  console.log(user)
  const [expense,setExpense] = useState([])
  const [expenseType, setExpenseType] = useState([])
  useEffect(()=>{
     const local = localStorage.getItem('token')
    fetch('http://localhost:4000/api/ledger/display', {

                    headers: {

                       Authorization: `Bearer ${local}`
                    }
                })
    .then(res => res.json())
    .then(data => {
      
      console.log(data)
      setExpense(data)

      const tempExpenseArray = data.filter(data => data.user === user.id).filter(category => {
        if(category.type === "Expense"){

          return true

        }

      })

      console.log(tempExpenseArray)
       setExpenseType(tempExpenseArray)
    })

  },[])


console.log(expenseType)



  

  return (
    <>
     <MonthlyExpense rawData={expenseType} />
    </>
  )
}
