
import {useEffect,useState,useContext} from 'react'
import {Table,Button} from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../../UserContext'
import MonthlyIncome from '../../components/MonthlyIncome'


export default function income(){

  const {user} = useContext(UserContext) 

  console.log(user)
  const [income,setIncome] = useState([])
  const [incomeType, setIncomeType] = useState([])
  useEffect(()=>{
     const local = localStorage.getItem('token')
    fetch('http://localhost:4000/api/ledger/display', {

                    headers: {

                       Authorization: `Bearer ${local}`
                    }
                })
    .then(res => res.json())
    .then(data => {
      
      console.log(data)
      setIncome(data)

      const tempIncomeArray = data.filter(data => data.user === user.id).filter(category => {
        if(category.type === "Income"){

          return true

        }

      })

      console.log(tempIncomeArray)
       setIncomeType(tempIncomeArray)
    })

  },[])


console.log(incomeType)



  

  return (
    <>
     <MonthlyIncome rawData={incomeType} />
    </>
  )
}
