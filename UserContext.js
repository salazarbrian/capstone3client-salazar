import React from 'react'

/*
	Context 
	
	In React.js, context is a way to pass data through the component tree without having to pass props manually.

	Context can be a way for us to create a "global" user state that components can share and update.

	We're going to create a userContext so that we can share the data of the user between our components.

*/

const UserContext = React.createContext()

/*
	Provider

	Every context object comes with a Provider React component.
	This allows components to subscribe to changes made to the context.
*/

export const UserProvider = UserContext.Provider

export default UserContext	
